# Objects:

**Purpose:** Style objects that will show up on any page or context such that the context doesn't have to clarify how the objects work within them unless it's unique to the block or page.
**Preferred load order:** Traditionally alphabetical, order agnostic
**Global load order:** Fourth, after page layouts
// General
const gulp = require("gulp");
const plumber = require("gulp-plumber");
const browserSync = require("browser-sync").create();

// CSS
const compass = require("gulp-compass");
const autoprefixer = require("gulp-autoprefixer");
const uglify = require("gulp-clean-css");

gulp.task("css", () => {
  return gulp
    .src("sass/**/*.sass")
    .pipe(compass({
      sass: "sass",
      css: "css"
    }))
    .pipe(autoprefixer({
      browsers: ["last 5 versions", "ie >= 11", "> 5%"],
      cascade: false,
      grid: true
    }))
    .pipe(plumber())
    .pipe(uglify())
    .pipe(gulp.dest("css"))
    .pipe(browserSync.stream());
});

gulp.task("browser-sync", ["css"], () => {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });

  gulp.watch("sass/**/*.sass", ["css"]);
});

gulp.task("default", ["browser-sync"]);

$(document).ready(function () {
  // When clicking a menu item, make sure it's the only one that's active
  $('.menu-item').click(function () {
    $('.menu-item').removeClass("active");
    $(this).addClass("active");
  });

  // Scroll to anchor links on click and don't add to URL
  $('a[href*="#"]').click(function (e) {
    e.preventDefault();

    $('html, body').animate({
      scrollTop: $($(this).attr("href")).offset().top - 16
    }, 200)

    $($(this).attr("href")).removeClass("collapsed");
  
    $(this).parents('.aside-nav').find('.collapse-btn').text("Collapse all");
  });

  // Collapse cards
  $('.collapse-btn').click(function (e) {
    e.preventDefault();

    if ($('.site-body .card').length === $('.site-body .card.collapsed').length) {
      $('.site-body .card').removeClass("collapsed");
  
      $(this).text("Collapse all");
    } else {
      $('.site-body .card').addClass("collapsed");
  
      $(this).text("Uncollapse all");
    }
  });

  // Add anchor link to clipboard
  $('.card-title').click(function () {
    $('#copyField').val(window.location + "#" + $(this).text());
    $('#copyField').select();
    
    document.execCommand("copy");
  });
});
